package br.com.guesthotel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="CATEGORIAITEMRESTAURANTE")
public class CategoriaItemRestaurante implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idCategoriaItemRestaurante;
	@Column(length=20)
	private String nome;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	private Boolean ativo;
	@ManyToOne
	@JoinColumn(name="idHotel")
	private Hotel hotel;
	
	public CategoriaItemRestaurante() {
		// TODO Auto-generated constructor stub
	}

	public CategoriaItemRestaurante(String nome, Date dataCadastro, Boolean ativo) {
		super();
		this.nome = nome;
		this.dataCadastro = dataCadastro;
		this.ativo = ativo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Long getIdCategoriaItemRestaurante() {
		return idCategoriaItemRestaurante;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCategoriaItemRestaurante == null) ? 0 : idCategoriaItemRestaurante.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CategoriaItemRestaurante other = (CategoriaItemRestaurante) obj;
		if (idCategoriaItemRestaurante == null) {
			if (other.idCategoriaItemRestaurante != null)
				return false;
		} else if (!idCategoriaItemRestaurante.equals(other.idCategoriaItemRestaurante))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CategoriaItemRestaurante [idCategoriaItemRestaurante=" + idCategoriaItemRestaurante + ", nome=" + nome
				+ ", dataCadastro=" + dataCadastro + ", ativo=" + ativo + ", hotel=" + hotel + "]";
	}
	
	

}
