package br.com.guesthotel.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.guesthotel.enumerator.StatusPedido;
@Entity
@Table(name="ITEMPEDIDORESTAURANTE")
public class ItemPedidoRestaurante implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idItemPedidoRestaurante;
	@OneToOne
	@JoinColumn(name="idItemRestaurante")
	private ItemRestaurante itemRestaurante;
	@OneToOne
	@JoinColumn(name="idPedidoRestaurante")
	private PedidoRestaurante pedidoRestaurante;	
	private StatusPedido pedido;
	private Integer quantidade;
	private Double preco;
	
	public ItemPedidoRestaurante() {
		// TODO Auto-generated constructor stub
	}

	public ItemPedidoRestaurante(StatusPedido pedido, Integer quantidade, Double preco) {
		super();
		this.pedido = pedido;
		this.quantidade = quantidade;
		this.preco = preco;
	}

	public ItemRestaurante getItemRestaurante() {
		return itemRestaurante;
	}

	public void setItemRestaurante(ItemRestaurante itemRestaurante) {
		this.itemRestaurante = itemRestaurante;
	}

	public PedidoRestaurante getPedidoRestaurante() {
		return pedidoRestaurante;
	}

	public void setPedidoRestaurante(PedidoRestaurante pedidoRestaurante) {
		this.pedidoRestaurante = pedidoRestaurante;
	}

	public StatusPedido getPedido() {
		return pedido;
	}

	public void setPedido(StatusPedido pedido) {
		this.pedido = pedido;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public Long getIdItemPedidoRestaurante() {
		return idItemPedidoRestaurante;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idItemPedidoRestaurante == null) ? 0 : idItemPedidoRestaurante.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemPedidoRestaurante other = (ItemPedidoRestaurante) obj;
		if (idItemPedidoRestaurante == null) {
			if (other.idItemPedidoRestaurante != null)
				return false;
		} else if (!idItemPedidoRestaurante.equals(other.idItemPedidoRestaurante))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ItemPedidoRestaurante [idItemPedidoRestaurante=" + idItemPedidoRestaurante + ", itemRestaurante="
				+ itemRestaurante + ", pedidoRestaurante=" + pedidoRestaurante + ", pedido=" + pedido + ", quantidade="
				+ quantidade + ", preco=" + preco + "]";
	}
	
	
	
	
	
}
