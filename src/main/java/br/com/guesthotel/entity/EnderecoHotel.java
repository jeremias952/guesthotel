package br.com.guesthotel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name="ENDERECOHOTEL")
public class EnderecoHotel implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@OneToOne
	@JoinColumn(name="idHotel")
	private Hotel hotel;
	@Column(length=8)
	private String cep;
	@Column(length=60)
	private String endereco;
	@Column(length=10)
	private String numero;
	@Column(length=30)
	private String bairro;
	@ManyToOne
	@JoinColumn(name="idCidade",referencedColumnName="idCidade")
	private Cidade cidade;
	
	public EnderecoHotel() {
		// TODO Auto-generated constructor stub
	}

	

	public EnderecoHotel(Hotel hotel, String cep, String endereco, String numero, String bairro, Cidade cidade) {
		super();
		this.hotel = hotel;
		this.cep = cep;
		this.endereco = endereco;
		this.numero = numero;
		this.bairro = bairro;
		this.cidade = cidade;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hotel == null) ? 0 : hotel.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnderecoHotel other = (EnderecoHotel) obj;
		if (hotel == null) {
			if (other.hotel != null)
				return false;
		} else if (!hotel.equals(other.hotel))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "EnderecoHotel [hotel=" + hotel + ", cep=" + cep + ", endereco=" + endereco + ", numero=" + numero
				+ ", bairro=" + bairro + ", cidade=" + cidade + "]";
	}

	
	
}
