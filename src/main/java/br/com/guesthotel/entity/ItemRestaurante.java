package br.com.guesthotel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="ITEMRESTAURANTE")
public class ItemRestaurante implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idItemRestaurante;
	@Column(length=30)
	private String nome;
	@Column(length=500)
	private String descricao;
	private Double preco;
	@Column(length=50)
	private String nomeImagem;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	private Boolean ativo;
	@ManyToOne
	@JoinColumn(name="idCategoriaItemRestaurante")
	private CategoriaItemRestaurante categoriaItemRestaurante;
	@ManyToOne
	@JoinColumn(name="idHotel")
	private Hotel hotel;
	
	public ItemRestaurante() {
		// TODO Auto-generated constructor stub
	}

	public ItemRestaurante(String nome, String descricao, Double preco, String nomeImagem, Date dataCadastro,
			Boolean ativo) {
		super();
		this.nome = nome;
		this.descricao = descricao;
		this.preco = preco;
		this.nomeImagem = nomeImagem;
		this.dataCadastro = dataCadastro;
		this.ativo = ativo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public String getNomeImagem() {
		return nomeImagem;
	}

	public void setNomeImagem(String nomeImagem) {
		this.nomeImagem = nomeImagem;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public CategoriaItemRestaurante getCategoriaItemRestaurante() {
		return categoriaItemRestaurante;
	}

	public void setCategoriaItemRestaurante(CategoriaItemRestaurante categoriaItemRestaurante) {
		this.categoriaItemRestaurante = categoriaItemRestaurante;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Long getIdItemRestaurante() {
		return idItemRestaurante;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idItemRestaurante == null) ? 0 : idItemRestaurante.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemRestaurante other = (ItemRestaurante) obj;
		if (idItemRestaurante == null) {
			if (other.idItemRestaurante != null)
				return false;
		} else if (!idItemRestaurante.equals(other.idItemRestaurante))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ItemRestaurante [idItemRestaurante=" + idItemRestaurante + ", nome=" + nome + ", descricao=" + descricao
				+ ", preco=" + preco + ", nomeImagem=" + nomeImagem + ", dataCadastro=" + dataCadastro + ", ativo="
				+ ativo + ", categoriaItemRestaurante=" + categoriaItemRestaurante + ", hotel=" + hotel + "]";
	}
	
	

}
