package br.com.guesthotel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="HOTEL")
public class Hotel implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idHotel;
	@Column(length=60)
	private String nome;	
	private Long cpf;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	private Boolean ativo;
	
	public Hotel() {
		// TODO Auto-generated constructor stub
	}

	public Hotel(Long idHotel, String nome, Long cpf, Date dataCadastro, Boolean ativo) {
		super();
		this.idHotel = idHotel;
		this.nome = nome;
		this.cpf = cpf;
		this.dataCadastro = dataCadastro;
		this.ativo = ativo;
	}

	public Long getIdHotel() {
		return idHotel;
	}

	public void setIdHotel(Long idHotel) {
		this.idHotel = idHotel;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getCpf() {
		return cpf;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idHotel == null) ? 0 : idHotel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Hotel other = (Hotel) obj;
		if (idHotel == null) {
			if (other.idHotel != null)
				return false;
		} else if (!idHotel.equals(other.idHotel))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Hotel [idHotel=" + idHotel + ", nome=" + nome + ", cpf=" + cpf + ", dataCadastro=" + dataCadastro
				+ ", ativo=" + ativo + "]";
	}
	
	
	
}
