package br.com.guesthotel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="TOKEN")
public class Token implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idToken;
	@Column(length=5)
	private String codigoAcesso;
	@Column(length=10)
	private String uh;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataInicio;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFim;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	@ManyToOne
	@JoinColumn(name="idHotel")
	private Hotel hotel;
	@ManyToOne
	@JoinColumn(name="idUsuarioAdmin")
	private UsuarioAdmin usuarioAdmin;
	@ManyToOne
	@JoinColumn(name="idUsuarioHospede")
	private UsuarioHospede usuarioHospede;
	
	public Token() {
		// TODO Auto-generated constructor stub
	}

	public Token(String codigoAcesso, String uh, Date dataInicio, Date dataFim, Date dataCadastro) {
		super();
		this.codigoAcesso = codigoAcesso;
		this.uh = uh;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.dataCadastro = dataCadastro;
	}

	public String getCodigoAcesso() {
		return codigoAcesso;
	}

	public void setCodigoAcesso(String codigoAcesso) {
		this.codigoAcesso = codigoAcesso;
	}

	public String getUh() {
		return uh;
	}

	public void setUh(String uh) {
		this.uh = uh;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public UsuarioAdmin getUsuarioAdmin() {
		return usuarioAdmin;
	}

	public void setUsuarioAdmin(UsuarioAdmin usuarioAdmin) {
		this.usuarioAdmin = usuarioAdmin;
	}

	public UsuarioHospede getUsuarioHospede() {
		return usuarioHospede;
	}

	public void setUsuarioHospede(UsuarioHospede usuarioHospede) {
		this.usuarioHospede = usuarioHospede;
	}

	public Long getIdToken() {
		return idToken;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idToken == null) ? 0 : idToken.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Token other = (Token) obj;
		if (idToken == null) {
			if (other.idToken != null)
				return false;
		} else if (!idToken.equals(other.idToken))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Token [idToken=" + idToken + ", codigoAcesso=" + codigoAcesso + ", uh=" + uh + ", dataInicio="
				+ dataInicio + ", dataFim=" + dataFim + ", dataCadastro=" + dataCadastro + ", hotel=" + hotel
				+ ", usuarioAdmin=" + usuarioAdmin + ", usuarioHospede=" + usuarioHospede + "]";
	}
	
	

}
