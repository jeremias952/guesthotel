package br.com.guesthotel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name="TELEFONEHOTEL")
public class TelefoneHotel implements Serializable {

	private static final long serialVersionUID = 1L;	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@OneToOne
	@JoinColumn(name="idHotel")
	private Hotel hotel;	
	private Short ddd;
	private Integer numero;
	@Column(length=60)
	private String responsavel;
	
	public TelefoneHotel() {
		// TODO Auto-generated constructor stub
	}

	public TelefoneHotel(Hotel hotel, Short ddd, Integer numero, String responsavel) {
		super();
		this.hotel = hotel;
		this.ddd = ddd;
		this.numero = numero;
		this.responsavel = responsavel;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Short getDdd() {
		return ddd;
	}

	public void setDdd(Short ddd) {
		this.ddd = ddd;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hotel == null) ? 0 : hotel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TelefoneHotel other = (TelefoneHotel) obj;
		if (hotel == null) {
			if (other.hotel != null)
				return false;
		} else if (!hotel.equals(other.hotel))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TelefoneHotel [hotel=" + hotel + ", ddd=" + ddd + ", numero=" + numero + ", responsavel=" + responsavel
				+ "]";
	}
	
	

}
