package br.com.guesthotel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.guesthotel.enumerator.StatusPedido;

@Entity
@Table(name="PEDIDORESTAURANTE")
public class PedidoRestaurante implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idPedidoRestaurante;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro; 
	private StatusPedido statusPedido;
	@Column(length=500)
	private String observacao;
	@ManyToOne
	@JoinColumn(name="idUsuarioHospede")
	private UsuarioHospede usuarioHospede;
	@ManyToOne
	@JoinColumn(name="idHotel")
	private Hotel hotel;
	@ManyToOne
	@JoinColumn(name="idUsuarioAdmin")
	private UsuarioAdmin usuarioAdmin;
	
	public PedidoRestaurante() {
		// TODO Auto-generated constructor stub
	}

	public PedidoRestaurante(Date dataCadastro, StatusPedido statusPedido, String observacao) {
		super();
		this.dataCadastro = dataCadastro;
		this.statusPedido = statusPedido;
		this.observacao = observacao;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public StatusPedido getStatusPedido() {
		return statusPedido;
	}

	public void setStatusPedido(StatusPedido statusPedido) {
		this.statusPedido = statusPedido;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public UsuarioHospede getUsuarioHospede() {
		return usuarioHospede;
	}

	public void setUsuarioHospede(UsuarioHospede usuarioHospede) {
		this.usuarioHospede = usuarioHospede;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public UsuarioAdmin getUsuarioAdmin() {
		return usuarioAdmin;
	}

	public void setUsuarioAdmin(UsuarioAdmin usuarioAdmin) {
		this.usuarioAdmin = usuarioAdmin;
	}

	public Long getIdPedidoRestaurante() {
		return idPedidoRestaurante;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPedidoRestaurante == null) ? 0 : idPedidoRestaurante.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PedidoRestaurante other = (PedidoRestaurante) obj;
		if (idPedidoRestaurante == null) {
			if (other.idPedidoRestaurante != null)
				return false;
		} else if (!idPedidoRestaurante.equals(other.idPedidoRestaurante))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PedidoRestaurante [idPedidoRestaurante=" + idPedidoRestaurante + ", dataCadastro=" + dataCadastro
				+ ", statusPedido=" + statusPedido + ", observacao=" + observacao + ", usuarioHospede=" + usuarioHospede
				+ ", hotel=" + hotel + ", usuarioAdmin=" + usuarioAdmin + "]";
	}
	
	

}
