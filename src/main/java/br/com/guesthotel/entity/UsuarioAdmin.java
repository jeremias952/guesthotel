package br.com.guesthotel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="USUARIOADMIN")
public class UsuarioAdmin implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idUsuarioAdmin;
	@Column(length=60)
	private String nome;
	@Column(length=80)
	private String email;
	@Column(length=60)
	private String senha;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	private Boolean ativo;
	@ManyToOne
	@JoinColumn(name="idHotel")
	private Hotel hotel;
	
	public UsuarioAdmin() {
		// TODO Auto-generated constructor stub
	}

	public UsuarioAdmin(Long idUsuarioAdmin, String nome, String email, String senha, Date dataCadastro, Boolean ativo,
			Hotel hotel) {
		super();
		this.idUsuarioAdmin = idUsuarioAdmin;
		this.nome = nome;
		this.email = email;
		this.senha = senha;
		this.dataCadastro = dataCadastro;
		this.ativo = ativo;
		this.hotel = hotel;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Long getIdUsuarioAdmin() {
		return idUsuarioAdmin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idUsuarioAdmin == null) ? 0 : idUsuarioAdmin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioAdmin other = (UsuarioAdmin) obj;
		if (idUsuarioAdmin == null) {
			if (other.idUsuarioAdmin != null)
				return false;
		} else if (!idUsuarioAdmin.equals(other.idUsuarioAdmin))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UsuarioAdmin [idUsuarioAdmin=" + idUsuarioAdmin + ", nome=" + nome + ", email=" + email + ", senha="
				+ senha + ", dataCadastro=" + dataCadastro + ", ativo=" + ativo + ", hotel=" + hotel + "]";
	}
	
	

}
