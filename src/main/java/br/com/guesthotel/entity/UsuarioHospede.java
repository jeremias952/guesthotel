package br.com.guesthotel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.guesthotel.enumerator.Sexo;

@Entity
@Table(name="USUARIOHOSPEDE")
public class UsuarioHospede implements Serializable {

	private static final long serialVersionUID = 1L;	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idUsuarioHospede;
	@Column(length=60)
	private String nome;	
	private Sexo sexo;
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;
	private Integer cpf;
	@Column(length=80)
	private String email;
	@Column(length=60)
	private String senha;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	private Boolean ativo;
	@ManyToOne
	@JoinColumn(name="idCidade")
	private Cidade cidade;
	
	public UsuarioHospede() {
		// TODO Auto-generated constructor stub
	}

	public UsuarioHospede(String nome, Sexo sexo, Date dataNascimento, Integer cpf, String email, String senha,
			Date dataCadastro, Boolean ativo) {
		super();
		this.nome = nome;
		this.sexo = sexo;
		this.dataNascimento = dataNascimento;
		this.cpf = cpf;
		this.email = email;
		this.senha = senha;
		this.dataCadastro = dataCadastro;
		this.ativo = ativo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Integer getCpf() {
		return cpf;
	}

	public void setCpf(Integer cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Long getIdUsuarioHospede() {
		return idUsuarioHospede;
	}	

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idUsuarioHospede == null) ? 0 : idUsuarioHospede.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioHospede other = (UsuarioHospede) obj;
		if (idUsuarioHospede == null) {
			if (other.idUsuarioHospede != null)
				return false;
		} else if (!idUsuarioHospede.equals(other.idUsuarioHospede))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UsuarioHospede [idUsuarioHospede=" + idUsuarioHospede + ", nome=" + nome + ", sexo=" + sexo
				+ ", dataNascimento=" + dataNascimento + ", cpf=" + cpf + ", email=" + email + ", senha=" + senha
				+ ", dataCadastro=" + dataCadastro + ", ativo=" + ativo + "]";
	}
	
	

}
