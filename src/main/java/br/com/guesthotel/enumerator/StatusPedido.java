package br.com.guesthotel.enumerator;

public enum StatusPedido {
	
	PARALIZADO("Paralizado"),EM_EXECUCAO("Em Execucao"),FINALIZADO("Finalizado");
	
	private String value;

	private StatusPedido(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

}
